package editor;

import java.awt.GridLayout;

import javax.swing.JCheckBox;
import javax.swing.JPanel;

public class CSVImportSwitch extends JPanel {
	JCheckBox extra;

	JCheckBox create;

	public CSVImportSwitch() {
		super(new GridLayout(0, 1));
		extra = new JCheckBox("Extra Players");
		create = new JCheckBox("Created Players");
		// head.setToolTipText("
		add(extra);
		add(create);
	}

}
